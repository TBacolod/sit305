package com.example.task81c;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class MainActivity extends YouTubeBaseActivity {
    YouTubePlayerView mYoutubePlayerView;
    Button playBtn;
    YouTubePlayer.OnInitializedListener mOnInitializedListener;
    String youtubeVideoURL;
    EditText youtubeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        playBtn = (Button) findViewById(R.id.playButton);
        youtubeEditText = (EditText) findViewById(R.id.youtubeVideoText);
        mYoutubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeView);

        mOnInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(youtubeVideoURL);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (youtubeEditText.getText().length() > 0) {
                    youtubeVideoURL = youtubeEditText.getText().toString();
                    String[] cutURL = youtubeVideoURL.split("\\=");
                    if (cutURL.length == 3) {
                        String[] tempCut = cutURL[1].split("\\&");
                        youtubeVideoURL = tempCut[0];
                    } else {
                        youtubeVideoURL = cutURL[1];
                    }
                    mYoutubePlayerView.initialize(YouTubeConfig.getApiKey(), mOnInitializedListener);
                } else {
                    Toast.makeText(MainActivity.this, "Please enter a Youtube Video URL", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}