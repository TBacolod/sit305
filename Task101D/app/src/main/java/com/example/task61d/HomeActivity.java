package com.example.task61d;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.CursorWindow;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.task61d.data.AllFoodDataBaseHelper;
import com.example.task61d.data.CartDataBaseHelper;
import com.example.task61d.data.FoodDataBaseHelper;
import com.example.task61d.data.UserDataBaseHelper;
import com.example.task61d.model.FoodItem;
import com.google.android.material.navigation.NavigationView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements MainViewAdapter.OnFoodItemListener, NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ImageButton listButton;
    RecyclerView mainRecyclerView;
    UserDataBaseHelper db;
    FoodDataBaseHelper  fdb;
    AllFoodDataBaseHelper afdb;
    MainViewAdapter mainFoodListViewAdapter;
    String homeUsername;
    TextView textTest;
    List<FoodItem> menuFoodItemList = new ArrayList<>();
    List<FoodItem> menufoodList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mainRecyclerView = findViewById(R.id.mainListView);
        textTest = findViewById(R.id.textView2);
        db = new UserDataBaseHelper(this);
        fdb = new FoodDataBaseHelper(this);
        afdb = new AllFoodDataBaseHelper(this);
        homeUsername = getIntent().getExtras().getString("userdata");
        textTest.setText("Welcome " + db.fetchFullName(homeUsername) + "!");

        mainFoodListViewAdapter = new MainViewAdapter(HomeActivity.this, menuFoodItemList, this);
        mainRecyclerView.setAdapter(mainFoodListViewAdapter);

        RecyclerView.LayoutManager foodListLayoutManager = new LinearLayoutManager(this);
        mainRecyclerView.setLayoutManager(foodListLayoutManager);

        menufoodList = afdb.showAllFoods();
        for (FoodItem food : menufoodList)
        {
            menuFoodItemList.add(food);
        }

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        listButton = findViewById(R.id.newListButton);

        setSupportActionBar(toolbar);

        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newlistIntent = new Intent(HomeActivity.this, AddFoodActivity.class);
                newlistIntent.putExtra("userdata", homeUsername);
                startActivity(newlistIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        navigationView.setCheckedItem(R.id.nav_home);
        super.onResume();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                break;
            case R.id.account:
                Intent accountintent = new Intent(HomeActivity.this, AccountActivity.class);
                accountintent.putExtra("userdata", homeUsername);
                startActivity(accountintent);
                break;
            case R.id.listings:
                Intent listintent = new Intent(HomeActivity.this, ListActivity.class);
                listintent.putExtra("userdata", homeUsername);
                startActivityForResult(listintent, 1);
                break;
            case R.id.cart:
                Intent cartintent = new Intent(HomeActivity.this, CartActivity.class);
                cartintent.putExtra("userdata", homeUsername);
                startActivity(cartintent);
                break;
            case R.id.logout:
                Intent logoutintent = new Intent(this, LoginActivity.class);
                logoutintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(logoutintent);
                finish();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFoodItemClick(int position) {
        Bundle bundle = new Bundle();
        String fullname = db.fetchFullName(menuFoodItemList.get(position).getUsername());
        bundle.putString("fullname", fullname);
        bundle.putString("username", homeUsername);
        bundle.putString("foodName", menuFoodItemList.get(position).getTitle());
        bundle.putString("foodDesc", menuFoodItemList.get(position).getDescription());
        bundle.putString("foodQuantity", menuFoodItemList.get(position).getQuantity());
        bundle.putString("foodLocation", menuFoodItemList.get(position).getLocation());
        bundle.putString("foodTime", menuFoodItemList.get(position).getTime());
        bundle.putString("foodDate", menuFoodItemList.get(position).getDate());
        bundle.putByteArray("foodImage", menuFoodItemList.get(position).getImage());
        bundle.putDouble("foodLatitude", menuFoodItemList.get(position).getLatitude());
        bundle.putDouble("foodLongitude", menuFoodItemList.get(position).getLongitude());
        Fragment fragment;
        fragment = new MenuFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment2,fragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        menuFoodItemList.clear();
        mainFoodListViewAdapter = new MainViewAdapter(HomeActivity.this, menuFoodItemList, this);
        mainRecyclerView.setAdapter(mainFoodListViewAdapter);

        RecyclerView.LayoutManager foodListLayoutManager = new LinearLayoutManager(this);
        mainRecyclerView.setLayoutManager(foodListLayoutManager);

        menufoodList = afdb.showAllFoods();
        for (FoodItem food : menufoodList)
        {
            menuFoodItemList.add(food);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}