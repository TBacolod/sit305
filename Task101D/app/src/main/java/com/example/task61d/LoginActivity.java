package com.example.task61d;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task61d.data.AllFoodDataBaseHelper;
import com.example.task61d.data.FoodDataBaseHelper;
import com.example.task61d.data.UserDataBaseHelper;
import com.example.task61d.model.User;
import com.example.task61d.util.Util;

public class LoginActivity extends AppCompatActivity {
    Button signup, login;
    UserDataBaseHelper db;
    FoodDataBaseHelper fdb;
    AllFoodDataBaseHelper afdb;
    EditText usernameEntry, passwordEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signup = findViewById(R.id.signupButton);
        login = findViewById(R.id.loginButton);
        usernameEntry = findViewById(R.id.usernameEntryText);
        passwordEntry = findViewById(R.id.passwordEntryText);
        db = new UserDataBaseHelper(this);
        afdb = new AllFoodDataBaseHelper(this);
        fdb = new FoodDataBaseHelper(this);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createIntent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(createIntent);
            }
        });

        login.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check to see if user exists
                boolean result = db.fetchUser(usernameEntry.getText().toString(), passwordEntry.getText().toString());
                if (result == true)
                {
                    Toast.makeText(LoginActivity.this, "Successfully logged in!", Toast.LENGTH_SHORT).show();
                    Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                    homeIntent.putExtra("userdata", usernameEntry.getText().toString());
                    startActivity(homeIntent);
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "The user does not exist.", Toast.LENGTH_SHORT).show();
                }
            }
        }));
    }
}