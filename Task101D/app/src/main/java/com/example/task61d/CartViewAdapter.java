package com.example.task61d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task61d.model.CartItem;

import java.util.List;

public class CartViewAdapter extends RecyclerView.Adapter<CartViewAdapter.ViewHolder>{
    private Context context;
    private List<CartItem> cartItemList;

    public CartViewAdapter (Context context, List<CartItem> cartItemList) {
        this.context = context;
        this.cartItemList = cartItemList;
    }
    @NonNull
    @Override
    public CartViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_layout, parent, false);
        return new CartViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewAdapter.ViewHolder holder, int position) {
        holder.titleText.setText(cartItemList.get(position).getTitle());
        holder.priceText.setText(String.format("$%.2f",cartItemList.get(position).getPrice()));
    }

    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleText, priceText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //Set view IDs
            titleText = itemView.findViewById(R.id.foodText);
            priceText = itemView.findViewById(R.id.costText);
        }
    }
}
