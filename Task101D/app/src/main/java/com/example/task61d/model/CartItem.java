package com.example.task61d.model;

public class CartItem {
    private String title;
    private String username;
    private double price;

    public CartItem(){}

    public CartItem(String username, String title, double price) {
        this.username = username;
        this.title = title;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
