package com.example.task61d.data;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import androidx.annotation.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.task61d.model.FoodItem;
import com.example.task61d.util.FoodUtil;
import com.example.task61d.util.Util;

public class FoodDataBaseHelper extends SQLiteOpenHelper {

    public FoodDataBaseHelper(@Nullable Context context) {
        super(context, FoodUtil.DATABASE_NAME, null, FoodUtil.DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + FoodUtil.TABLE_NAME  + "(" + FoodUtil.FOOD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + FoodUtil.FOOD_IMAGE + " blob,"
                + FoodUtil.USERNAME + " TEXT, " + FoodUtil.FOOD_TITLE + " TEXT, " + FoodUtil.FOOD_DESC + " TEXT, " + FoodUtil.FOOD_LOCATION + " TEXT, " + FoodUtil.QUANTITY + " TEXT, "
                + FoodUtil.FOOD_TIME + "  TEXT, " + FoodUtil.FOOD_DATE + " TEXT, " + FoodUtil.FOOD_LATITUDE + " DOUBLE, " + FoodUtil.FOOD_LONGITUDE + " DOUBLE)";
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USER_TABLE = "DROP TABLE IF EXISTS";
        db.execSQL(DROP_USER_TABLE, new String[]{FoodUtil.TABLE_NAME});
        onCreate(db);
    }


    //Insert note method
    public long insertFood (FoodItem foodContent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(FoodUtil.FOOD_IMAGE, foodContent.getImage());
        contentValues.put(FoodUtil.USERNAME, foodContent.getUsername()); //Does not need ID input as it auto increment
        contentValues.put(FoodUtil.FOOD_TITLE, foodContent.getTitle());
        contentValues.put(FoodUtil.FOOD_DESC, foodContent.getDescription());
        contentValues.put(FoodUtil.FOOD_LOCATION, foodContent.getLocation());
        contentValues.put(FoodUtil.QUANTITY, foodContent.getQuantity());
        contentValues.put(FoodUtil.FOOD_TIME, foodContent.getTime());
        contentValues.put(FoodUtil.FOOD_DATE, foodContent.getDate());
        contentValues.put(FoodUtil.FOOD_LATITUDE, foodContent.getLatitude());
        contentValues.put(FoodUtil.FOOD_LONGITUDE, foodContent.getLongitude());
        long newRowID = db.insert(FoodUtil.TABLE_NAME, null, contentValues); //If null, does not insert row
       db.close();
        return newRowID;
    }

    //Delete Note Method
    public void deleteFood (String stringContent) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(" DELETE FROM " + FoodUtil.TABLE_NAME + " WHERE " + FoodUtil.FOOD_TITLE + "=\"" + stringContent.toString() + "\";" );
        db.close();
    }

    //Create Empty list and populate with each Database row
    public List<FoodItem> showUserFoods(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(FoodUtil.TABLE_NAME, null, null, null,  null, null, null);
        List<FoodItem> allFood = new ArrayList<>();
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(FoodUtil.USERNAME));
                if (nameMatch.equals(username))
                {
                    FoodItem food = new FoodItem(cursor.getBlob(cursor.getColumnIndex(FoodUtil.FOOD_IMAGE)),cursor.getString(cursor.getColumnIndex(FoodUtil.USERNAME)),cursor.getString(cursor.getColumnIndex(FoodUtil.FOOD_TITLE)), cursor.getString(cursor.getColumnIndex(FoodUtil.FOOD_DESC)),
                            cursor.getString(cursor.getColumnIndex(FoodUtil.FOOD_LOCATION)), cursor.getString(cursor.getColumnIndex(FoodUtil.QUANTITY)), cursor.getString(cursor.getColumnIndex(FoodUtil.FOOD_TIME)),
                                    cursor.getString(cursor.getColumnIndex(FoodUtil.FOOD_DATE)), cursor.getDouble(cursor.getColumnIndex(FoodUtil.FOOD_LATITUDE)), cursor.getDouble(cursor.getColumnIndex(FoodUtil.FOOD_LONGITUDE)));
                    allFood.add(food);
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return allFood;
    }

    //Show all users' usernames in database
    public ArrayList<String> allUsers() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(FoodUtil.TABLE_NAME, null, null, null,  null, null, null);
        ArrayList<String> usernames = new ArrayList<>();
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                usernames.add(cursor.getString(cursor.getColumnIndex(FoodUtil.USERNAME)));
            } while (cursor.moveToNext());
        }
        db.close();
        return usernames;
    }

    //Search if username exists in database
    public String fetchUserName (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(FoodUtil.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(FoodUtil.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(FoodUtil.USERNAME)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }
}
