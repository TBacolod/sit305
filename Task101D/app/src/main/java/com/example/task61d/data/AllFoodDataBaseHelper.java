package com.example.task61d.data;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import androidx.annotation.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.task61d.model.CartItem;
import com.example.task61d.model.FoodItem;
import com.example.task61d.util.FoodUtil;
import com.example.task61d.util.MainUtil;
import com.example.task61d.util.Util;

public class AllFoodDataBaseHelper extends SQLiteOpenHelper {

    public AllFoodDataBaseHelper(@Nullable Context context) {
        super(context, MainUtil.DATABASE_NAME, null, MainUtil.DATABASE_VERSION);
    }
    @Override

    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + MainUtil.TABLE_NAME  + "(" + MainUtil.FOOD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + MainUtil.FOOD_IMAGE + " blob,"
                + MainUtil.USERNAME + " TEXT, " + MainUtil.FOOD_TITLE + " TEXT, " + MainUtil.FOOD_DESC + " TEXT, " + MainUtil.FOOD_LOCATION + " TEXT, " + MainUtil.QUANTITY + " TEXT, "
                + MainUtil.FOOD_TIME + "  TEXT, " + MainUtil.FOOD_DATE + " TEXT, " + MainUtil.FOOD_LATITUDE + " DOUBLE, " + MainUtil.FOOD_LONGITUDE + " DOUBLE)";
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USER_TABLE = "DROP TABLE IF EXISTS";
        db.execSQL(DROP_USER_TABLE, new String[]{MainUtil.TABLE_NAME});
        onCreate(db);
    }


    //Insert note method
    public long insertFood (FoodItem foodContent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MainUtil.FOOD_IMAGE, foodContent.getImage());
        contentValues.put(MainUtil.USERNAME, foodContent.getUsername()); //Does not need ID input as it auto increment
        contentValues.put(MainUtil.FOOD_TITLE, foodContent.getTitle());
        contentValues.put(MainUtil.FOOD_DESC, foodContent.getDescription());
        contentValues.put(MainUtil.FOOD_LOCATION, foodContent.getLocation());
        contentValues.put(MainUtil.QUANTITY, foodContent.getQuantity());
        contentValues.put(MainUtil.FOOD_TIME, foodContent.getTime());
        contentValues.put(MainUtil.FOOD_DATE, foodContent.getDate());
        contentValues.put(MainUtil.FOOD_LATITUDE, foodContent.getLatitude());
        contentValues.put(MainUtil.FOOD_LONGITUDE, foodContent.getLongitude());
        long newRowID = db.insert(MainUtil.TABLE_NAME, null, contentValues); //If null, does not insert row
        db.close();
        return newRowID;
    }

    //Create Empty list and populate with each Database row
    public List<FoodItem> showAllFoods() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(MainUtil.TABLE_NAME, null, null, null,  null, null, null);
        List<FoodItem> allFood = new ArrayList<>();
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();

            }
            do {
                FoodItem food = new FoodItem(cursor.getBlob(cursor.getColumnIndex(MainUtil.FOOD_IMAGE)),cursor.getString(cursor.getColumnIndex(MainUtil.USERNAME)),cursor.getString(cursor.getColumnIndex(MainUtil.FOOD_TITLE)), cursor.getString(cursor.getColumnIndex(MainUtil.FOOD_DESC)),
                        cursor.getString(cursor.getColumnIndex(MainUtil.FOOD_LOCATION)), cursor.getString(cursor.getColumnIndex(MainUtil.QUANTITY)), cursor.getString(cursor.getColumnIndex(MainUtil.FOOD_TIME)),
                        cursor.getString(cursor.getColumnIndex(MainUtil.FOOD_DATE)), cursor.getDouble(cursor.getColumnIndex(MainUtil.FOOD_LATITUDE)), cursor.getDouble(cursor.getColumnIndex(MainUtil.FOOD_LONGITUDE)));
                allFood.add(food);
            } while (cursor.moveToNext());
        }
        db.close();
        return allFood;
    }

}
