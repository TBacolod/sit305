package com.example.task61d;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task61d.data.FoodDataBaseHelper;
import com.example.task61d.model.FoodItem;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class AddFoodActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    TextView timeText, dateText;
    int tHour, tMinute;
    ImageView tImageView;
    Button tImageBtn, listBtn;
    ImageButton backBtn;
    String tUsername, tLocation;
    Uri image;
    EditText tTitle, tDescription, tQuantity;
    double tLongitude, tLatitude;
    FoodDataBaseHelper fdb;
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);

        //Set view IDs
        tUsername = getIntent().getExtras().getString("userdata");
        tTitle = findViewById(R.id.titleText);
        tDescription = findViewById(R.id.descText);
        tQuantity = findViewById(R.id.quantityText);
        timeText = findViewById(R.id.timeText);
        dateText = findViewById(R.id.dateText);
        tImageView = findViewById(R.id.foodImage);
        tImageBtn = findViewById(R.id.imageButton);
        backBtn = findViewById(R.id.backButton);
        listBtn = findViewById(R.id.listButton);
        fdb = new FoodDataBaseHelper(this);


        //Initialise Place Picker Fragment
        Places.initialize(getApplicationContext(), getString(R.string.PLACES_API_KEY));
        PlacesClient placesClient = Places.createClient(this);

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                tLocation = place.getName();
                tLongitude = place.getLatLng().longitude;
                tLatitude = place.getLatLng().latitude;
            }


            @Override
            public void onError(@NonNull Status status) {
                // TODO: Handle the error.
                Log.i("Places", "An error occurred: " + status);
            }
        });


        //Image Button Picker
        tImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        //Request Permission
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        //Show popup
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else {
                        pickImageFromGallery();
                    }
                } else {
                    //System OS less than Marshmallow
                    pickImageFromGallery();
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Add Listing Button
        listBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = tTitle.getText().toString();
                String description = tDescription.getText().toString();
                String location = tLocation;
                String quantity = tQuantity.getText().toString();
                String time = timeText.getText().toString();
                String date = dateText.getText().toString();
                //Error check, make sure all fields are complete
                if ((title.length() > 0) && (description.length() > 0) && (tLocation != null) && (quantity.length() > 0) && (time.length() > 0) && (date.length() > 0)) {
                    try {
                        //Add food listing to database
                        fdb.insertFood(new FoodItem(imageViewToByte(tImageView), tUsername, title, description, location, quantity, time, date, tLatitude, tLongitude));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(AddFoodActivity.this, "Successfully added listing!", Toast.LENGTH_LONG).show();
                    setResult(1);
                    finish();
                } else {
                    Toast.makeText(AddFoodActivity.this, "Please complete entry on all fields!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Calendar Picker
        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });

        //Time picker
        timeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        AddFoodActivity.this, android.R.style.Theme_Dialog,
                        (view, hourOfDay, minute) -> {
                            tHour = hourOfDay;
                            tMinute = minute;
                            String time = tHour + ":" + tMinute;
                            SimpleDateFormat t24Hours = new SimpleDateFormat("HH:mm");
                            try {
                                Date date = t24Hours.parse(time);
                                SimpleDateFormat t12Hours = new SimpleDateFormat("hh:mm aa");
                                timeText.setText(t12Hours.format(date));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }, 12, 0, false );
                timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable((Color.parseColor("#66000000"))));
                timePickerDialog.updateTime(tHour,tMinute);
                timePickerDialog.show();
            }
        });
    }


    //Change date textbox to given calendar date
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance().format(calendar.getTime());

        dateText.setText(currentDateString);
    }

    //Convert imageView to Byte[] method
    private byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    //Image Picker intent
    private void pickImageFromGallery() {
        Intent intent = new Intent (Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    //Handle Result of Runtime Permission for selecting Image from Gallery
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery();
                }
                else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //Handle Result of Picked Image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            //Set Image to ImageView
            image = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(image);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                //Drawable myDrawable = Drawable.createFromStream(inputStream, image.toString());
                tImageView.setImageBitmap(bitmap);
            }
            catch (FileNotFoundException e) {
                tImageView.setImageResource(R.drawable.account_icon);
            }
        }
    }
}