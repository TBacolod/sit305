package com.example.task61d.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.task61d.util.Util;
import com.example.task61d.model.User;

import java.util.ArrayList;

public class UserDataBaseHelper extends SQLiteOpenHelper {
    public UserDataBaseHelper(@Nullable Context context) {
        super(context, Util.DATABASE_NAME, null, Util.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + Util.TABLE_NAME + "(" + Util.USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + Util.USERNAME + " TEXT, " + Util.PASSWORD + " TEXT, " + Util.FULL_NAME + " TEXT, " + Util.ADDRESS + " TEXT, " + Util.EMAIL + "  TEXT, " + Util.PHONE_NUMBER + " TEXT)";
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Util.TABLE_NAME);
        String CREATE_USER_TABLE = "CREATE TABLE " + Util.TABLE_NAME + "(" + Util.USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + Util.USERNAME + " TEXT, " + Util.PASSWORD + " TEXT, " + Util.FULL_NAME + " TEXT, " + Util.ADDRESS + " TEXT, " + Util.EMAIL + "  TEXT, " + Util.PHONE_NUMBER + " TEXT)";
        db.execSQL(CREATE_USER_TABLE);
    }

    //Insert User into databasse
    public long insertUser (User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Util.USERNAME, user.getUsername()); //Does not need ID input as it auto increments
        contentValues.put(Util.PASSWORD, user.getPassword());
        contentValues.put(Util.FULL_NAME, user.getFullname());
        contentValues.put(Util.ADDRESS, user.getHomeaddress());
        contentValues.put(Util.EMAIL, user.getEmailaddress());
        contentValues.put(Util.PHONE_NUMBER, user.getPhone());
        long newRowID = db.insert(Util.TABLE_NAME, null, contentValues); //If null, does not insert row
        db.close();
        return newRowID;
    }


    //Search database to see if input user exists with provided password
    public boolean fetchUser(String username, String password)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, new String[]{Util.USER_ID}, Util.USERNAME + "=? and " + Util.PASSWORD + "=?",
                new String[] {username, password}, null, null, null);
        int numberOfRows = cursor.getCount();
        db.close();

        if (numberOfRows > 0)
            return  true;
        else
            return false;
    }

    //Search database to see if input user exists with only username
    public boolean fetchUser(String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, new String[]{Util.USER_ID}, Util.USERNAME + "=?",
                new String[] {username}, null, null, null);
        int numberOfRows = cursor.getCount();
        db.close();

        if (numberOfRows > 0)
            return  true;
        else
            return false;
    }

    //Return all users and all user elements in database
    public ArrayList<String> allUsers() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        ArrayList<String> usernames = new ArrayList<>();
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.USERNAME)));
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.PASSWORD)));
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.FULL_NAME)));
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.ADDRESS)));
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.EMAIL)));
                usernames.add(cursor.getString(cursor.getColumnIndex(Util.PHONE_NUMBER)));
            } while (cursor.moveToNext());
        }
        db.close();
        return usernames;
    }

    //Return a single user's details
    public ArrayList<String> userDetails(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        ArrayList<String> userDetails = new ArrayList<>();
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                 {
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.USERNAME)));
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.PASSWORD)));
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.FULL_NAME)));
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.ADDRESS)));
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.EMAIL)));
                    userDetails.add(cursor.getString(cursor.getColumnIndex(Util.PHONE_NUMBER)));
                    break;
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return userDetails;
    }


    //Search database with given username and return username
    public String fetchUserName (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.USERNAME)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }

    //Search database with given username and return password
    public String fetchPassword (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.PASSWORD)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }

    //Search database with given username and return fullname
    public String fetchFullName (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.FULL_NAME)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }

    //Search database with given username and return address
    public String fetchAddress (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.ADDRESS)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }

    //Search database with given username and return email address
    public String fetchEmail (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.EMAIL)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }

    //Search database with given username and return phone number
    public String fetchPhoneNumber (String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(Util.USERNAME));
                if (nameMatch.equals(name))
                {
                    return (cursor.getString(cursor.getColumnIndex(Util.PHONE_NUMBER)));
                }
            } while (cursor.moveToNext());
        }
        db.close();
        return "";
    }
}
