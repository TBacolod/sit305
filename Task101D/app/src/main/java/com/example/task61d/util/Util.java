package com.example.task61d.util;

public class Util {
    public static final int  DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "user_db";
    public static final String TABLE_NAME = "users";
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String ADDRESS = "address";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String FULL_NAME = "full_name";
    public static final String EMAIL = "email";
}
