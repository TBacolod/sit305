package com.example.task61d.util;

public class MainUtil {
    public static final int  DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "share_db";
    public static final String TABLE_NAME = "shared_foods";
    public static final String FOOD_IMAGE = "img";
    public static final String FOOD_ID = "food_id";
    public static final String FOOD_TITLE = "food_title";
    public static final String FOOD_LOCATION = "food_location";
    public static final String FOOD_DESC = "food_description";
    public static final String QUANTITY = "food_quantity";
    public static final String FOOD_TIME = "food_time";
    public static final String FOOD_DATE = "food_date";
    public static final String FOOD_LONGITUDE = "food_longitude";
    public static final String FOOD_LATITUDE = "food_latitude";
    public static final String USERNAME = "username";
}
