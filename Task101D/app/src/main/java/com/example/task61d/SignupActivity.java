package com.example.task61d;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task61d.data.UserDataBaseHelper;
import com.example.task61d.model.User;

public class SignupActivity extends AppCompatActivity {
    Button signup;
    EditText username, password, confirmpassword, fullname, address, email, phone;
    UserDataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Set view IDs
        signup = findViewById(R.id.signupButton);
        username = findViewById(R.id.usernameText);
        password = findViewById(R.id.passwordText);
        confirmpassword = findViewById(R.id.confirmpasswordText);
        fullname = findViewById(R.id.fullnameText);
        address = findViewById(R.id.addressText);
        email = findViewById(R.id.emailText);
        phone = findViewById(R.id.phoneText);
        //Set new User database instance
        db = new UserDataBaseHelper(this);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sFullname = fullname.getText().toString();
                String sAddress = address.getText().toString();
                String sEmail = email.getText().toString();
                String sPhone = phone.getText().toString();
                String sUsername = username.getText().toString();
                String sPassword = password.getText().toString();
                String sConfirmPassword = confirmpassword.getText().toString();
                //Error check, make sure all fields are complete
                if ((sFullname.length() > 0) && (sAddress.length() > 0) && (sEmail.length() > 0) && (sPhone.length() > 0) && (sUsername.length() > 0)) {
                    if (sPassword.length() > 6) {
                        if (sPassword.equals(sConfirmPassword)) {
                            boolean existingUser = db.fetchUser(sUsername);
                            if (existingUser == false) {
                                long result = db.insertUser(new User(sUsername, sPassword, sFullname, sAddress, sEmail, sPhone));
                                if (result > 0) {
                                    Toast.makeText(SignupActivity.this, "Registered successfully!", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(SignupActivity.this, "User already exists!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else {
                                Toast.makeText(SignupActivity.this, "User already exists!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(SignupActivity.this, "Passwords do not match!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(SignupActivity.this, "Password is too short!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(SignupActivity.this, "Please complete entry on all fields!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}