package com.example.task61d.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.task61d.model.CartItem;
import com.example.task61d.util.CartUtil;
import com.example.task61d.util.FoodUtil;
import com.example.task61d.util.MainUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CartDataBaseHelper extends SQLiteOpenHelper {
    public CartDataBaseHelper(@Nullable Context context) {
        super(context, CartUtil.DATABASE_NAME, null, CartUtil.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CART_TABLE = "CREATE TABLE " + CartUtil.TABLE_NAME  + "(" + CartUtil.CART_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + CartUtil.USERNAME + " TEXT,"
                + CartUtil.FOOD_TITLE + " TEXT, " + CartUtil.FOOD_PRICE + " DOUBLE)";
        db.execSQL(CREATE_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USER_TABLE = "DROP TABLE IF EXISTS";
        db.execSQL(DROP_USER_TABLE, new String[]{CartUtil.TABLE_NAME});
        onCreate(db);
    }
    //Insert Item into Database
    public long insertPrice (CartItem cartContent) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CartUtil.USERNAME, cartContent.getUsername());
        contentValues.put(CartUtil.FOOD_TITLE, cartContent.getTitle());
        contentValues.put(CartUtil.FOOD_PRICE, cartContent.getPrice());
        long newRowID = db.insert(CartUtil.TABLE_NAME, null, contentValues); //If null, does not insert row
        //db.close();
        return newRowID;
    }

    //Show the cart list for given user name
    public List<CartItem> showUserPrice(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(CartUtil.TABLE_NAME, null, null, null,  null, null, null);
        List<CartItem> allPrice = new ArrayList<>();
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(CartUtil.USERNAME));
                if (nameMatch.equals(username))
                {
                    CartItem cart = new CartItem(cursor.getString(cursor.getColumnIndex(CartUtil.USERNAME)),
                            cursor.getString(cursor.getColumnIndex(CartUtil.FOOD_TITLE)), cursor.getDouble(cursor.getColumnIndex(CartUtil.FOOD_PRICE)));
                    allPrice.add(cart);
                }
            } while (cursor.moveToNext());
        }
        //db.close();
        return allPrice;
    }

    public double totalCost(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(CartUtil.TABLE_NAME, null, null, null,  null,null, null);
        double totalPrice = 0;
        String nameMatch;
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();
            }
            do {
                nameMatch = cursor.getString(cursor.getColumnIndex(CartUtil.USERNAME));
                if (nameMatch.equals(username))
                {
                    totalPrice += cursor.getDouble(cursor.getColumnIndex(CartUtil.FOOD_PRICE));
                }
            } while (cursor.moveToNext());
        }
        //db.close();
        return totalPrice;
    }
}
