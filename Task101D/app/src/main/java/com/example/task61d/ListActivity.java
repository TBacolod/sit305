package com.example.task61d;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.CursorWindow;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.example.task61d.data.FoodDataBaseHelper;
import com.example.task61d.model.FoodItem;
import com.google.android.material.navigation.NavigationView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity implements ListViewAdapter.OnFoodItemListener, NavigationView.OnNavigationItemSelectedListener{
    List<FoodItem> foodItemList = new ArrayList<>();
    List<FoodItem> foodList;
    FoodDataBaseHelper fdb;
    RecyclerView listView;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ListViewAdapter foodListViewAdapter;
    ImageButton listButton2;
    String listUsername;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //Set view IDs
        drawerLayout = findViewById(R.id.list_drawer_layout);
        navigationView = findViewById(R.id.list_nav_view);
        toolbar = findViewById(R.id.toolbar3);
        listButton2 = findViewById(R.id.newListButton2);
        listView = findViewById(R.id.foodListView);
        listUsername = getIntent().getExtras().getString("userdata");

        //Create new instance of Food Database
        fdb = new FoodDataBaseHelper(this);

        //Set top tool bar with navigation bar
        setSupportActionBar(toolbar);
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //Set Listings tab to be checked in Navigation Bar
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.listings);

        //Set list view adapter to custom ListViewAdapter
        foodListViewAdapter = new ListViewAdapter(ListActivity.this, foodItemList, this);
        listView.setAdapter(foodListViewAdapter);

        //Set layout manager
        RecyclerView.LayoutManager foodListLayoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(foodListLayoutManager);

        //Show all food listed by user
        foodList = fdb.showUserFoods(listUsername);
        for (FoodItem food : foodList)
        {
            foodItemList.add(food);
        }

        //Add Food Activity
        listButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newlistIntent = new Intent(ListActivity.this, AddFoodActivity.class);
                newlistIntent.putExtra("userdata", listUsername);
                startActivityForResult(newlistIntent, 1);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            //Close existing drawer
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (fragment != null) {
            //Close existing fragment
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        } else {
            super.onBackPressed();
        }
    }

    //Navigation Drawer Elements Clicked
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home: //Sets Home as Only Activity on Backstack
                Intent homeintent = new Intent(this, HomeActivity.class);
                homeintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                setResult(1);
                startActivity(homeintent);
                break;
            case R.id.account: //Account Page
                Intent accountintent = new Intent(ListActivity.this, AccountActivity.class);
                accountintent.putExtra("userdata", listUsername);
                startActivity(accountintent);
                break;
            case R.id.listings: //Current Page
                break;
            case R.id.cart: //Cart Page
                Intent cartintent = new Intent(ListActivity.this, CartActivity.class);
                cartintent.putExtra("userdata", listUsername);
                startActivity(cartintent);
                break;
            case R.id.logout: //Logout Page
                Intent logoutintent = new Intent(this, LoginActivity.class);
                logoutintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(logoutintent);
                finish();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //When clicking Food Item from List
    @Override
    public void onFoodItemClick(int position) {
        Bundle bundle = new Bundle();
        //Put all data from database into bundle
        bundle.putString("username", listUsername);
        bundle.putString("foodName", foodItemList.get(position).getTitle());
        bundle.putString("foodDesc", foodItemList.get(position).getDescription());
        bundle.putString("foodQuantity", foodItemList.get(position).getQuantity());
        bundle.putString("foodLocation", foodItemList.get(position).getLocation());
        bundle.putString("foodTime", foodItemList.get(position).getTime());
        bundle.putString("foodDate", foodItemList.get(position).getDate());
        bundle.putByteArray("foodImage", foodItemList.get(position).getImage());
        bundle.putDouble("foodLatitude", foodItemList.get(position).getLatitude());
        bundle.putDouble("foodLongitude", foodItemList.get(position).getLongitude());
        //Creating of food fragment
        Fragment fragment;
        fragment = new ListingFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment,fragment).commit();
    }

    //Update Listview when Successfully adding new Listing
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        foodItemList.clear();

        //Set list view adapter to custom ListViewAdapter
        foodListViewAdapter = new ListViewAdapter(ListActivity.this, foodItemList, this);
        listView.setAdapter(foodListViewAdapter);

        //Set layout manager
        RecyclerView.LayoutManager foodListLayoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(foodListLayoutManager);

        //Show all food listed by user
        foodList = fdb.showUserFoods(listUsername);
        for (FoodItem food : foodList)
        {
            foodItemList.add(food);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}