package com.example.task61d;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task61d.data.CartDataBaseHelper;
import com.example.task61d.data.UserDataBaseHelper;
import com.example.task61d.model.CartItem;
import com.example.task61d.model.FoodItem;
import com.example.task61d.model.User;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {
    String cartUsername;
    ImageButton backBtn;
    Button buyBtn;
    TextView userText, priceText, emptyText;
    UserDataBaseHelper db;
    CartViewAdapter cartViewAdapter;
    CartDataBaseHelper cdb;
    List<CartItem> cartItemList = new ArrayList<>();
    RecyclerView cartlistView;
    final int UPI_PAYMENT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        //Set view IDs
        cartUsername = getIntent().getExtras().getString("userdata");
        backBtn = findViewById(R.id.backButton3);
        buyBtn = findViewById(R.id.buyButton);
        userText = findViewById(R.id.textView13);
        cartlistView = findViewById(R.id.cartListView);
        emptyText = findViewById(R.id.textView16);
        priceText = findViewById(R.id.priceText);

        //Intialise Databases
        cdb = new CartDataBaseHelper(this);
        db = new UserDataBaseHelper(this);

        priceText.setText(String.format("$%.2f", (cdb.totalCost(cartUsername))));
        userText.setText("Hi " + db.fetchFullName(cartUsername));

        //Fill recycler list
        cartItemList = cdb.showUserPrice(cartUsername);

        cartViewAdapter = new CartViewAdapter(CartActivity.this, cartItemList);
        cartlistView.setAdapter(cartViewAdapter);

        RecyclerView.LayoutManager cartListLayoutManager = new LinearLayoutManager(this);
        cartlistView.setLayoutManager(cartListLayoutManager);

        if (cartViewAdapter.getItemCount() > 0)
        {
            emptyText.setVisibility(View.INVISIBLE);
            buyBtn.setVisibility(View.VISIBLE);

            buyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    payUsingUPI(String.valueOf(cdb.totalCost(cartUsername)), "tristanrulez01@gmail.com", cartUsername, "Payment of Cart Goods");
                }
            });
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void payUsingUPI(String amount, String UpiID, String name, String note) {
        Uri uri = Uri.parse("upi://pay").buildUpon()
                .appendQueryParameter("pa",UpiID)
                .appendQueryParameter("pn",name)
                .appendQueryParameter("tn",note)
                .appendQueryParameter("am",amount)
                .appendQueryParameter("cu","INR")
                .build();

        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
        upiPayIntent.setData(uri);

        //Will show a dialog to user to choose an app
        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");

        //Check if intent resolves
        if (null != chooser.resolveActivity(getPackageManager())) {
            startActivityForResult(chooser, UPI_PAYMENT);
        } else {
            Toast.makeText(CartActivity.this, "No UPI app found, please install one to continue", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case UPI_PAYMENT:
                if ((RESULT_OK == resultCode) || (resultCode == 11)) {
                    if (data != null) {
                        String text = data.getStringExtra("response");
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add(text);
                        upiPaymentDataOperation(dataList);
                    } else {
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add("nothing");
                        upiPaymentDataOperation(dataList);
                    }
                } else {
                    ArrayList<String> dataList = new ArrayList<>();
                    dataList.add("nothing");
                    upiPaymentDataOperation(dataList);
                }
                break;
         }
    }

    private void upiPaymentDataOperation(ArrayList<String> data) {
        if (isConnectionAvailable(CartActivity.this)) {
            String str = data.get(0);
            String paymentCancel = "";
            if (str == null) str = "discard";
            String status = "";
            String approvalRefNo = "";
            String response[] = str.split("&");
            for (int i = 0; i < response.length; i++) {
                String equalsStr[] = response[i].split("=");
                if (equalsStr.length >= 2) {
                    if (equalsStr[0].toLowerCase().equals("Status".toLowerCase())) {
                        status = equalsStr[1].toLowerCase();
                    } else if (equalsStr[0].toLowerCase().equals("ApprovalRefNo".toLowerCase()) || equalsStr[0].toLowerCase().equals("txnRef".toLowerCase())) {
                        approvalRefNo = equalsStr[1];
                    }
                } else {
                    paymentCancel = "Payment cancelled by user.";
                }
            }
            if (status.equals("success")) {
                Toast.makeText(CartActivity.this, "Transaction Successful.", Toast.LENGTH_SHORT).show();
            } else if ("Payment cancelled by user.".equals(paymentCancel)) {
                Toast.makeText(CartActivity.this, "Payment cancelled by user.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (connectivityManager.getActiveNetwork() != null)
            {
                return true;
            }
        }
        return false;
    }
}