package com.example.task61d.util;

public class CartUtil {
    public static final int  DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "cart_db";
    public static final String TABLE_NAME = "cart_lists";
    public static final String CART_ID = "cart_id";
    public static final String FOOD_PRICE = "food_price";
    public static final String USERNAME = "username";
    public static final String FOOD_TITLE = "food_title";
}
