package com.example.task61d;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task61d.data.CartDataBaseHelper;
import com.example.task61d.model.CartItem;
import com.example.task61d.model.FoodItem;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance(String param1, String param2) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CartDataBaseHelper cdb;
        cdb = new CartDataBaseHelper(getContext());
        TextView background;
        CardView informationCard;
        Button map, cart;

        //Get bundle elements
        String username = getArguments().getString("username");
        String fullname = getArguments().getString("fullname");
        String foodName = getArguments().getString("foodName");
        String foodDescription = getArguments().getString("foodDesc");
        String foodQuantity = getArguments().getString("foodQuantity");
        String foodLocation = getArguments().getString("foodLocation");
        String foodDate = getArguments().getString("foodDate");
        String foodTime = getArguments().getString("foodTime");
        byte[] foodImage = getArguments().getByteArray("foodImage");
        Double foodLongitude = getArguments().getDouble("foodLongitude");
        Double foodLatitude = getArguments().getDouble("foodLatitude");

        LayoutInflater lf = getActivity().getLayoutInflater();
        View view = lf.inflate(R.layout.fragment_menu, container, false);

        //Set view IDs and textbox texts
        map = view.findViewById(R.id.showMapButton);
        cart = view.findViewById(R.id.cartButton);
        background = view.findViewById(R.id.backgroundZone2);
        informationCard = view.findViewById(R.id.infoCard2);
        TextView foodText = view.findViewById(R.id.fragFoodTitle2);
        foodText.setText(foodName);
        TextView descText = view.findViewById(R.id.fragFoodDesc2);
        descText.setText(foodDescription);
        ImageView imageText = view.findViewById(R.id.fragFoodImage2);
        TextView quantityText = view.findViewById(R.id.fragFoodQuantity2);
        quantityText.setText("Quantity: " + foodQuantity);
        TextView locationText = view.findViewById(R.id.fragFoodLocation2);
        locationText.setText(foodLocation);
        TextView dateText = view.findViewById(R.id.fragFoodDate2);
        dateText.setText(foodDate);
        TextView timeText = view.findViewById(R.id.fragFoodTime2);
        timeText.setText(foodTime);
        TextView usernameText = view.findViewById(R.id.fragFoodUsername);
        usernameText.setText(fullname);
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        imageText.setImageBitmap(bitmap);

        informationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //Add item to cart
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insert into cart list
                cdb.insertPrice(new CartItem(username, foodName, 5.00));
                Toast.makeText(getContext(), "Successfully added to cart.", Toast.LENGTH_SHORT).show();
                container.removeView(view);
            }
        });

        //Show location on map with given name, latitude, longitude
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(getContext(), MapsActivity.class);
                mapIntent.putExtra("latitude",foodLatitude);
                mapIntent.putExtra("longitude", foodLongitude);
                mapIntent.putExtra("title", foodLocation);
                startActivity(mapIntent);
            }
        });

        //Exits fragment view when background is clicked
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container.removeView(view);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }
}