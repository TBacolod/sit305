package com.example.tourism;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PlacesViewAdapter extends RecyclerView.Adapter<PlacesViewAdapter.ViewHolder> {
    private Context context;
    private List<Places> placesList;
    private OnPlacesListener mOnPlacesListener;

    public PlacesViewAdapter(Context context, List<Places> placesList, OnPlacesListener onPlacesListener) {
        this.context = context;
        this.placesList = placesList;
        this.mOnPlacesListener = onPlacesListener;
    }

    @NonNull
    @Override
    public PlacesViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.places, parent, false);
        return new PlacesViewAdapter.ViewHolder(itemView, mOnPlacesListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewAdapter.ViewHolder holder, int position) {
        holder.name.setText(placesList.get(position).getName());
        holder.description.setText(placesList.get(position).getDescription());
        holder.image.setImageResource(placesList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return placesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, description;
        ImageView image;
        OnPlacesListener onPlacesListener;

        public ViewHolder(@NonNull View itemView, OnPlacesListener onPlacesListener) {
            super(itemView);
            name = itemView.findViewById(R.id.placesText);
            image = itemView.findViewById(R.id.imageView2);
            description = itemView.findViewById(R.id.descriptionText);
            this.onPlacesListener = onPlacesListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnPlacesListener.onPlacesClick(getAdapterPosition());
        }
    }

    public interface OnPlacesListener {
        void onPlacesClick(int position);
    }
}
