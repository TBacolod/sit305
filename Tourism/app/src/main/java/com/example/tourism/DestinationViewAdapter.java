package com.example.tourism;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DestinationViewAdapter extends RecyclerView.Adapter<DestinationViewAdapter.ViewHolder>{
    private Context context;
    private List<Destination> destinationList;
    private OnDestinationListener mOnDestinationListener;

    public DestinationViewAdapter(Context context, List<Destination> destinationList, OnDestinationListener onDestinationListener) {
        this.context = context;
        this.destinationList = destinationList;
        this.mOnDestinationListener = onDestinationListener;
    }
    @NonNull
    @Override
    public DestinationViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.destination, parent, false);
        return new ViewHolder(itemView, mOnDestinationListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DestinationViewAdapter.ViewHolder holder, int position) {
        holder.name.setText(destinationList.get(position).getName());
        holder.image.setImageResource(destinationList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return destinationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name;
        ImageView image;
        OnDestinationListener onDestinationListener;

        public ViewHolder(@NonNull View itemView, OnDestinationListener onDestinationListener) {
            super(itemView);
            name = itemView.findViewById(R.id.textView);
            image = itemView.findViewById(R.id.imageView);
            this.onDestinationListener = onDestinationListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onDestinationListener.onDestinationClick(getAdapterPosition());
        }
    }

    public interface OnDestinationListener {
        void onDestinationClick(int position);
    }
}
