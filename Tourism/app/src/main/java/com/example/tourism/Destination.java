package com.example.tourism;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class Destination {
    private String sName; //Can instatiate to private so unaccessible
    private int image;
    private int id;

    public Destination(int id, String name, int image)
    {
        this.id = id;
        this.image = image;
        this.sName = name;
    }

    public int getID(){return id;}
    public String getName() { return sName; }
    public int getImage() {
        return image;
    }
    public void setID (int id) { this.id = id; }
    public void setName (String name) { sName = name; }
    public void setImage (int image) {
        this.image = image;
    }
}
