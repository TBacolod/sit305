package com.example.tourism;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DestinationViewAdapter.OnDestinationListener, PlacesViewAdapter.OnPlacesListener {
    private static final String TAG = "test";
    List<Destination> destinationList = new ArrayList<>();
    List<Places> placesList = new ArrayList<>();;
    RecyclerView destinationView, placesView;
    DestinationViewAdapter destinationViewAdapter;
    PlacesViewAdapter placesViewAdapter;
    int destinationPosition;

    String[] destinationNameList = {"Bora Bora", "Paris", "San Francisco", "Sydney", "Banff", "Grand Canyon", "Greece"};
    Integer[] destinationImageList = {R.drawable.borabora, R.drawable.paris, R.drawable.sanfransisco, R.drawable.sydney, R.drawable.banff, R.drawable.grandcanyon, R.drawable.greece};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        placesView = findViewById(R.id.placesView);
        destinationView = findViewById(R.id.destinationView);
        destinationViewAdapter = new DestinationViewAdapter(MainActivity.this, destinationList, this);
        destinationView.setAdapter(destinationViewAdapter);

        placesView = findViewById(R.id.placesView);

        RecyclerView.LayoutManager destinationLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        destinationView.setLayoutManager(destinationLayoutManager);
        RecyclerView.LayoutManager placesLayoutManager = new LinearLayoutManager(this);
        placesView.setLayoutManager(placesLayoutManager);

        placesViewAdapter = new PlacesViewAdapter(MainActivity.this, placesList,this);
        placesView.setAdapter(placesViewAdapter);
        if (savedInstanceState != null) { //on Screen Rotate
            int resetView = savedInstanceState.getInt("position"); //Last position
            onDestinationClick(resetView);
        }
        else {
            placesList.add(new Places(-1, "Welcome!", "Click on a Destination to view Places to Visit", R.drawable.map));
        }
        for (int i = 0; i < destinationNameList.length; i++)
        {
            Destination destination = new Destination(i, destinationNameList[i], destinationImageList[i]);
            destinationList.add(destination);
        }
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment2);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
            onDestinationClick(destinationPosition);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putInt("position", destinationPosition);
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onDestinationClick(int position) {
        placesList = new ArrayList<>();
        placesViewAdapter = new PlacesViewAdapter(MainActivity.this, placesList, this);
        placesView.setAdapter(placesViewAdapter);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment2);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();

        }
        switch (position) {
            case 0:
                //Bora Bora
                placesList.add(new Places(1, "Matira Beach", "Picturesque beach offers scenic hiking trails and tourist services including several boutique shops, the popular Ben's Place snack bar and restaurants.", R.drawable.bora1));
                placesList.add(new Places(2, "Mount Otemanu", "This jagged remnant of an ancient volcano rises up sharply more than 2,400 feet from the surface of a sparkling, turquoise lagoon.", R.drawable.bora2));
                placesList.add(new Places(3, "Bora Bora Lagoonarium", "See a diverse range of tropical fish swimming in their habitat within this naturally enclosed section of the lagoon.", R.drawable.bora3));
                placesList.add(new Places(4, "Bora Bora Turtle Centre", "An aquatic centre for turtles.", R.drawable.bora4));
                destinationPosition = 0; //For landscape savedInstanced
                break;
            case 1:
                //Paris
                placesList.add(new Places(1, "Eiffel Tower", "Completed in 1889, this colossal landmark, although initially hated by many Parisians, is now a famous symbol of French civic pride.", R.drawable.paris3));
                placesList.add(new Places(2, "Arc de Triomphe", "The Arc de triomphe was begun in 1806, on the orders of Napoleon I to honour the victories of his Grande Armée. Inspired by the great arches of antiquity, the monument combines the commemorative with the symbolic and it has always played a major role in the national republican consciousness.", R.drawable.paris2));
                placesList.add(new Places(3, "Louvre Museum", "Home to Leonardo da Vinci's Mona Lisa, the Louvre is considered the world's greatest art museum, with an unparalleled collection of items covering the full spectrum of art through the ages.", R.drawable.paris1));
                placesList.add(new Places(4, "Palais Garnier", "This performance hall hosts opera, ballet and chamber music performances.", R.drawable.paris4));
                placesList.add(new Places(5, "Notre-Dam Cathedral", "This famous cathedral, a masterpiece of Gothic architecture on which construction began in the 12th centry, stands on the Ile de la Cite and is the symbolic heart of the city.", R.drawable.paris5));
                destinationPosition = 1;
                break;
            case 2:
                //San Francisco
                placesList.add(new Places(1, "Alcatraz Island", "Park rangers conduct tours by recounting the prison's thrilling history along with intriguing anecdotes about Al Capone and other legendary figures that made a \"home\" here.", R.drawable.sanfran1));
                placesList.add(new Places(2, "Golden Gate Bridge", "Stretching 4,200 feet and towering as high as a 65-story building, this well-known bridge is the gateway to San Francisco.", R.drawable.sanfran2));
                placesList.add(new Places(3, "Palace of Fine Arts", "Designed to look like a Roman ruin, this golden building is certainly a beauty to behold.", R.drawable.sanfran3));
                placesList.add(new Places(4, "Pier 39", "At Pier 39, there are shops, restaurants, a video arcade, street performances, the Aquarium of the Bay, virtual 3D rides, and views of California sea lions hauled out on docks on Pier 39's marina.", R.drawable.sanfran4));
                placesList.add(new Places(5, "Oracle Park", "Home of the San Francisco Giants Major League baseball team.", R.drawable.sanfran5));
                destinationPosition = 2;
                break;
            case 3:
                //Sydney
                placesList.add(new Places(1, "Sydney Opera House", "The Sydney Opera House is a multi-venue performing arts centre at Sydney Harbour located in Sydney, New South Wales, Australia. It is one of the 20th century's most famous and distinctive buildings.", R.drawable.sydney1));
                placesList.add(new Places(2, "Harbour Bridge", "The Sydney Harbour Bridge is an Australian heritage-listed steel through arch bridge across Sydney Harbour that carries rail, vehicular, bicycle, and pedestrian traffic between the Sydney central business district and the North Shore.", R.drawable.sydney2));
                placesList.add(new Places(3, "Bondi Beach", "The sweeping white-sand crescent of Bondi is one of Australia’s most iconic beaches. Reliable waves draw surfers while, nearby, hardy locals swim in the Icebergs ocean pool year-round.", R.drawable.sydney3));
                destinationPosition = 3;
                break;
            case 4:
                //Banff
                placesList.add(new Places(1, "Lake Louise", "Lake Louise is a hamlet in Banff National Park in the Canadian Rockies, known for its turquoise, glacier-fed lake ringed by high peaks and overlooked by a stately chateau.", R.drawable.banff1));
                placesList.add(new Places(2, "Banff Gondola", "Soar to the top of Sulphur Mountain to experience a stunning bird's-eye view of six incredible mountain ranges. With breathtaking vistas in every direction, your adventure begins with an eight-minute journey to the summit of Sulphur Mountain.", R.drawable.banff2));
                placesList.add(new Places(3, "Bow Falls", "Bow Falls is a major waterfall on the Bow River, Alberta just before the junction of it and the Spray River. They are located near the Banff Springs Hotel and golf course on the left-hand side of River Road.", R.drawable.banff3));
                destinationPosition = 4;
                break;
            case 5:
                //Grand Canyon
                placesList.add(new Places(1, "Grand Canyon South Rim", "The most developed area of Grand Canyon National Park, the South Rim offers amenities such as bus service, hotels and water stations, but is also more crowded than the North Rim. Scenic highlights include Pipe Creek Vista and Yavapai Point.", R.drawable.gc1));
                placesList.add(new Places(2, "South Kaibab Trail", "One of the two superhighways into the Canyon, the other being the Bright Angel Trail, this path runs along a ridge offering stunning views. During the summer, visitors are advised to bring at least two quarts of water along because it can get very hot and the trail offers no shade.", R.drawable.gc2));
                destinationPosition = 5;
                break;
            case 6:
                //Greece
                placesList.add(new Places(1, "Parthenon", "The Parthenon is a former temple on the Athenian Acropolis, Greece, dedicated to the goddess Athena, whom the people of Athens considered their patron.", R.drawable.greece1));
                placesList.add(new Places(2, "Acropolis of Athens", "The Acropolis of Athens is an ancient citadel located on a rocky outcrop above the city of Athens and contains the remains of several ancient buildings of great architectural and historic significance, the most famous being the Parthenon.", R.drawable.greece2));
                placesList.add(new Places(3, "Temple of Zeus", "The Temple of Olympian Zeus, also known as the Olympieion or Columns of the Olympian Zeus, is a former colossal temple at the center of the Greek capital Athens.", R.drawable.greece3));
                placesList.add(new Places(4, "Meteora", "The Meteora is a rock formation in central Greece hosting one of the largest and most precipitously built complexes of Eastern Orthodox monasteries, second in importance only to Mount Athos.", R.drawable.greece4));
                destinationPosition = 6;
                break;
            default:
                break;
        }
    }

    @Override
    public void onPlacesClick(int position) {

        Bundle bundle = new Bundle();
        bundle.putString("placesName", placesList.get(position).getName());
        bundle.putString("placesDesc", placesList.get(position).getDescription());
        bundle.putInt("placesImage", placesList.get(position).getImage());
        Fragment fragment;
        fragment = new FragmentOne();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment2,fragment).commit();

        placesList = new ArrayList<>();
        placesViewAdapter = new PlacesViewAdapter(MainActivity.this, placesList, this);
        placesView.setAdapter(placesViewAdapter);
    }
}