package com.example.tourism;

public class Places {
    private String sName, sDescription; //Can instatiate to private so unaccessible
    private int id, image;

    public Places(int id, String name, String description, int image)
    {
        this.id = id;
        this.sDescription = description;
        this.sName = name;
        this.image = image;
    }

    public int getID(){ return this.id; }
    public String getName() {
        return sName;
    }
    public String getDescription() { return sDescription; }
    public int getImage() {
        return image;
    }
    public void setName (String name) { sName = name; }
    public void setDescription (String description) {
        sDescription = description;
    }
}
