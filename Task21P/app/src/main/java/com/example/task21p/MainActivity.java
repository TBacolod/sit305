package com.example.task21p;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageButton lengthButton, weightButton, tempButton;
    TextView resultOne, resultTwo, resultThree, measureOne, measureTwo, measureThree;
    EditText inputNumber;
    Spinner converterSpinner;

    public void weightFunction(View v) {
        String cSpinner = converterSpinner.getSelectedItem().toString();
        if (cSpinner.equals("Kilogram")) {
            long number = Long.parseLong(inputNumber.getText().toString());
            long gram = number * 1000;
            long ounce = (long) (number * 35.27396195);
            long pound = (long) (number * 2.20462262185);
            resultOne.setText(String.valueOf(gram));
            resultTwo.setText(String.valueOf(ounce));
            resultThree.setText(String.valueOf(pound));
            measureOne.setText("Gram");
            measureTwo.setText("Ounce (oz)");
            measureThree.setText("Pound (lb)");
        }
        else {
            Toast.makeText(MainActivity.this, "Please select correct conversion type", Toast.LENGTH_LONG).show();
        }
    }

    public void lengthFunction(View v) {
        String cSpinner = converterSpinner.getSelectedItem().toString();
        if (cSpinner.equals("Metre")) {
            long number = Long.parseLong(inputNumber.getText().toString());
            long centimetre = number * 100;
            double inch = number * 39.37007874;
            double foot = inch / 12;
            resultOne.setText(String.valueOf(centimetre));
            resultTwo.setText(String.format("%.2f", foot));
            resultThree.setText(String.format("%.2f", inch));
            measureOne.setText("Centimetre");
            measureTwo.setText("Foot");
            measureThree.setText("Inch");
        }
        else {
            Toast.makeText(MainActivity.this, "Please select correct conversion type", Toast.LENGTH_LONG).show();
        }
    }

    public void tempFunction(View v) {
        String cSpinner = converterSpinner.getSelectedItem().toString();
        if (cSpinner.equals("Celsius")) {
            long number = Long.parseLong(inputNumber.getText().toString());
            long fahrenheit = (long) (number * 1.8 + 32);
            long kelvin = (long) (number + 273.15);
            resultOne.setText(String.valueOf(fahrenheit));
            resultTwo.setText(String.valueOf(kelvin));
            resultThree.setText("");
            measureOne.setText("Fahrenheit");
            measureTwo.setText("Kelvin");
            measureThree.setText("");
        }
        else {
            Toast.makeText(MainActivity.this, "Please select correct conversion type", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputNumber = findViewById(R.id.inputNumber);
        lengthButton = findViewById(R.id.lengthButton);
        weightButton = findViewById(R.id.weightButton);
        tempButton = findViewById(R.id.tempButton);
        resultOne = findViewById(R.id.resultOne);
        resultTwo = findViewById(R.id.resultTwo);
        resultThree = findViewById(R.id.resultThree);
        measureOne = findViewById(R.id.measureOne);
        measureTwo = findViewById(R.id.measureTwo);
        measureThree = findViewById(R.id.measureThree);
        converterSpinner = findViewById(R.id.converterSpinner);
    }
}