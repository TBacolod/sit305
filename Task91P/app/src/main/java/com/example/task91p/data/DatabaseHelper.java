package com.example.task91p.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.task91p.model.Restaurant;
import com.example.task91p.util.Util;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(@Nullable Context context) {
        super(context, Util.DATABASE_NAME, null, Util.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RESTAURANT_TABLE = "CREATE TABLE " + Util.TABLE_NAME + "(" + Util.USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + Util.PLACE_NAME + " TEXT, " + Util.PLACE_LATITUDE + " DOUBLE, " + Util.PLACE_LONGITUDE + " DOUBLE)";
        db.execSQL(CREATE_RESTAURANT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USER_TABLE = "DROP TABLE IF EXISTS";
        db.execSQL(DROP_USER_TABLE, new String[]{Util.TABLE_NAME});

        onCreate(db);
    }

    public void insertRestaurant (Restaurant restaurant)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Util.PLACE_NAME, restaurant.getPlace_name());
        contentValues.put(Util.PLACE_LATITUDE, restaurant.getLatitude());
        contentValues.put(Util.PLACE_LONGITUDE, restaurant.getLongitude());
        db.insert(Util.TABLE_NAME, null, contentValues);
        //db.close();
    }

    public List<Restaurant> fetchRestaurants()
    {
        List<Restaurant> restaurantList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectAll = " SELECT * FROM " + Util.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectAll, null);

        if (cursor.moveToFirst()) {
            do {
                Restaurant restaurant = new Restaurant();
                restaurant.setPlace_name(cursor.getString(1));
                restaurant.setLatitude(cursor.getDouble(2));
                restaurant.setLongitude(cursor.getDouble(3));
                restaurantList.add(restaurant);
            } while (cursor.moveToNext());
        }

        //db.close();
        return restaurantList;
    }
}
