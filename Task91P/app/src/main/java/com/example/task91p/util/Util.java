package com.example.task91p.util;

public class Util {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "restaurant_db";
    public static final String TABLE_NAME = "restaurants";
    public static final String USER_ID = "user_id";
    public static final String PLACE_NAME = "place_name";
    public static final String PLACE_LONGITUDE = "place_longitude";
    public static final String PLACE_LATITUDE = "place_latitude";
}
