package com.example.task91p;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.example.task91p.data.DatabaseHelper;
import com.example.task91p.model.Restaurant;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    Bundle bundle;
    LatLng currentCords;
    String title;
    private GoogleMap mMap;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        db = new DatabaseHelper(MapsActivity.this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            currentCords = bundle.getParcelable("coordinates");
            title = bundle.getString("title");
        }


        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<Restaurant> restaurantList = db.fetchRestaurants();
        for (Restaurant restaurant : restaurantList)
        {
            LatLng place = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
            mMap.addMarker(new MarkerOptions().position(place).title(restaurant.getPlace_name()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 10));
        }

        if (bundle != null) {
            mMap.addMarker(new MarkerOptions().position(currentCords).title(title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentCords,15));
        }
    }
}