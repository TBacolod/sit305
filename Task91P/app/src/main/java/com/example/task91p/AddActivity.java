package com.example.task91p;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.task91p.data.DatabaseHelper;
import com.example.task91p.model.Restaurant;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddActivity extends AppCompatActivity {
    LocationManager locationManager;
    LocationListener locationListener;
    TextView placeText;
    Button currentBtn, showBtn, saveBtn;
    String currentTitle;
    Boolean isType = false;
    double latitude, longitude;
    double updateLat, updateLong;
    LatLng currentPosition;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        placeText = findViewById(R.id.placeText);
        currentBtn = findViewById(R.id.currentButton);
        showBtn = findViewById(R.id.showItemButton);
        saveBtn = findViewById(R.id.saveButton);
        db = new DatabaseHelper(AddActivity.this);

        // Initialize the SDK
        Places.initialize(getApplicationContext(), getString(R.string.Places_API));
        // Create a new PlacesClient instance
        PlacesClient placesClient = Places.createClient(this);

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.TYPES));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                // TODO: Get info about the selected place.
                currentTitle = place.getName();
                currentPosition = place.getLatLng();
                longitude = place.getLatLng().longitude;
                latitude = place.getLatLng().latitude;
                placeText.setText(currentTitle);
                String typeCheck = place.getTypes().toString();

                //Check if it is restaurant
                Pattern food = Pattern.compile("FOOD");
                Pattern restaurant = Pattern.compile("RESTAURANT");
                Matcher m = food.matcher(typeCheck);
                Matcher m1 = restaurant.matcher(typeCheck);
                if (m.find() == true || m1.find() == true) { //Check if input is of type FOOD or RESTAURANT
                    Log.i("typeCheck", "Found type");
                    isType = true;
                } else {
                    Log.i("typeCheck", "Not Found Type");
                    isType = false;
                }

                Log.i("Location", "Place: " + place.getName() + ", " + place.getId() +", " + place.getLatLng() + ", " + place.getTypes());
            }


            @Override
            public void onError(@NonNull Status status) {
                // TODO: Handle the error.
                Log.i("Location: ", "An error occurred: " + status);
            }
        });


        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                updateLat = location.getLatitude();
                updateLong= location.getLongitude();
            }
        };
        currentBtn.setOnClickListener(new View.OnClickListener() { //Current Location
            @Override
            public void onClick(View v) {
                currentTitle = "Current Location";
                latitude = updateLat;
                longitude = updateLong;
                currentPosition = new LatLng(latitude,longitude);
                placeText.setText(currentTitle);
                //Testing
                /*url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?location=" +
                       updateLat + "," + updateLong + "&radius=500&types=cafe&key=AIzaSyDGLvYLFnHAIgaoixrkCGs6zTeoan-eEgU";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString().trim(),Toast.LENGTH_SHORT).show();
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);*/
            }

        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPosition == null) {
                    Toast.makeText(AddActivity.this, "Please select a restaurant to view", Toast.LENGTH_SHORT).show();
                } else {
                    Intent mapIntent = new Intent(AddActivity.this, MapsActivity.class);
                    mapIntent.putExtra("coordinates",currentPosition);
                    mapIntent.putExtra("title",currentTitle);
                    startActivity(mapIntent);
                }
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPosition == null) { //If no Coordinates given
                    Toast.makeText(AddActivity.this, "Please select a restaurant to save", Toast.LENGTH_SHORT).show();
                } else {
                    if (isType == true) {  //Is a restaurant

                        Toast.makeText(AddActivity.this, "Successfully saved!", Toast.LENGTH_SHORT).show();
                        db.insertRestaurant(new Restaurant(currentTitle, latitude, longitude));
                        finish();
                    }
                    else {
                        Toast.makeText(AddActivity.this, "You can only saved Restaurants!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }
}