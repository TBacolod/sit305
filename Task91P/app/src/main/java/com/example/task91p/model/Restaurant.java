package com.example.task91p.model;

public class Restaurant {
    private int place_id;
    private double latitude;
    private double longitude;
    private String place_name;

    public Restaurant (String place_name, double latitude, double longitude) {
        this.place_name = place_name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Restaurant() {}

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }
}
