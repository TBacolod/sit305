package com.example.workouttimer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Handler;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Integer seconds;
    TextView timerText, infoText;
    EditText workoutType;
    Button startButton, pauseButton, stopButton;
    boolean timeRunning;
    SharedPreferences sharedPreferences;
    String workoutString, timerString;
    public static final String TIMER_VALUE = "timer";
    public static final String RUNNING_VALUE="running state";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        workoutType = findViewById(R.id.workoutType);
        startButton = findViewById(R.id.startButton);
        pauseButton = findViewById(R.id.pauseButton);
        stopButton = findViewById(R.id.stopButton);
        infoText = findViewById(R.id.infoText);
        timerText = findViewById(R.id.timerText);
        sharedPreferences = getSharedPreferences("com.example.workouttimer", MODE_PRIVATE);
        //sharedPreferences.edit().clear().apply();
        checkSharedPreferences();

        if (savedInstanceState != null)
        {
            timeRunning = savedInstanceState.getBoolean(RUNNING_VALUE); //Running state
            seconds = savedInstanceState.getInt(TIMER_VALUE); //Time value
        }
        else
        {
            seconds = 0;
            timeRunning = false;
        }
        Log.i("Time running", String.valueOf(timeRunning));
        runTimer();
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeRunning = true; //Start timer
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeRunning = false; //Stop timer
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeRunning = false;
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("key1", workoutType.getText().toString());
                editor.putString("key2", timerText.getText().toString());
                editor.commit(); //Commit key-value pairs
                seconds = 0; //Reset timer
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putBoolean(RUNNING_VALUE, timeRunning);
        outState.putInt(TIMER_VALUE, seconds);
        super.onSaveInstanceState(outState);

    }

    public void checkSharedPreferences() {
        workoutString = sharedPreferences.getString("key1", "");
        timerString = sharedPreferences.getString("key2", "");
        if (workoutString != "" || timerString !="") {
            if (workoutString != "") {
                String result = String.format("You spent %s on %s last session.", timerString, workoutString);
                infoText.setText(result);
            } else {
                String result = String.format("You worked out for %s last session.", timerString);
                infoText.setText(result);
            }
        }
        else
        {
            infoText.setText("Welcome to my Workout Timer App!");
        }
    }

    public void runTimer() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
           @Override
           public void run() {
               int minutes = seconds / 3600; //Rounded to next hour
               int secs = (seconds % 3600) / 60; //Rounded to next minute
               int millisecs = (seconds % 3600) % 60;

               String time = String.format(Locale.getDefault(), "%02d:%02d:%02d",minutes , secs, millisecs);
               timerText.setText(time);

               if (timeRunning) {
                   seconds++;
               }

               handler.postDelayed(this,1); //per second tick
           }
        });
    }
}