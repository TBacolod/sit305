package com.example.task61d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task61d.model.FoodItem;

import java.util.List;

public class MainViewAdapter extends RecyclerView.Adapter<MainViewAdapter.ViewHolder>{
    private Context context;
    private List<FoodItem> foodItemList;
    private MainViewAdapter.OnFoodItemListener mOnFoodItemListener;

    public MainViewAdapter(Context context, List<FoodItem> foodItemList, MainViewAdapter.OnFoodItemListener onFoodItemListener) {
        this.context = context;
        this.foodItemList = foodItemList;
        this.mOnFoodItemListener = onFoodItemListener;
    }

    @NonNull
    @Override
    public MainViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.main_list_layout, parent, false);
        return new MainViewAdapter.ViewHolder(itemView, mOnFoodItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewAdapter.ViewHolder holder, int position) {
        holder.title.setText(foodItemList.get(position).getTitle());
        holder.description.setText(foodItemList.get(position).getDescription());
        holder.time.setText(foodItemList.get(position).getTime());
        byte[] foodImage = foodItemList.get(position).getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        holder.image.setImageBitmap(bitmap);
        holder.location.setText(foodItemList.get(position).getLocation());
    }

    @Override
    public int getItemCount() {
        return foodItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title, description, location, time;
        ImageView image;
        MainViewAdapter.OnFoodItemListener onFoodItemListener;

        public ViewHolder(@NonNull View itemView, MainViewAdapter.OnFoodItemListener onFoodItemListener) {
            super(itemView);
            title = itemView.findViewById(R.id.titleListText);
            image = itemView.findViewById(R.id.imageListView);
            description = itemView.findViewById(R.id.descListText);
            location = itemView.findViewById(R.id.locationListText);
            time = itemView.findViewById(R.id.timeListText);
            this.onFoodItemListener = onFoodItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnFoodItemListener.onFoodItemClick(getAdapterPosition());
        }
    }

    public interface OnFoodItemListener {
        void onFoodItemClick(int position);
    }
}
