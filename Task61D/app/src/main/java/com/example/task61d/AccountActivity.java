package com.example.task61d;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task61d.data.UserDataBaseHelper;

import java.util.ArrayList;

public class AccountActivity extends AppCompatActivity {
    UserDataBaseHelper db;
    String homeUsername;
    ImageButton backBtn;
    TextView accountUserName, accountEmail, accountPhone, accountFullName, accountHomeAddress, accountPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        backBtn = findViewById(R.id.backButton2);

        db = new UserDataBaseHelper(this);
        accountUserName = findViewById(R.id.accountUserNameText);
        accountPassword = findViewById(R.id.accountPasswordText);
        accountFullName = findViewById(R.id.accountFullNameText);
        accountHomeAddress = findViewById(R.id.accountAddressText);
        accountEmail = findViewById(R.id.accountEmailText);
        accountPhone = findViewById(R.id.accountPhoneText);
        homeUsername = getIntent().getExtras().getString("userdata");

        accountUserName.setText(db.fetchUserName(homeUsername));
        accountPassword.setText(db.fetchPassword(homeUsername));
        accountFullName.setText(db.fetchFullName(homeUsername));
        accountHomeAddress.setText(db.fetchAddress(homeUsername));
        accountEmail.setText(db.fetchEmail(homeUsername));
        accountPhone.setText(db.fetchPhoneNumber(homeUsername));

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}