package com.example.task61d;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task61d.data.AllFoodDataBaseHelper;
import com.example.task61d.model.FoodItem;

import org.w3c.dom.Text;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListingFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private String file_path;

    public ListingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListingFragment newInstance(String param1, String param2) {
        ListingFragment fragment = new ListingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AllFoodDataBaseHelper afdb;
        afdb = new AllFoodDataBaseHelper(getContext());
        TextView background;
        CardView informationCard;
        String username = getArguments().getString("username");
        String foodName = getArguments().getString("foodName");
        String foodDescription = getArguments().getString("foodDesc");
        String foodQuantity = getArguments().getString("foodQuantity");
        String foodLocation = getArguments().getString("foodLocation");
        String foodDate = getArguments().getString("foodDate");
        String foodTime = getArguments().getString("foodTime");
        byte[] foodImage = getArguments().getByteArray("foodImage");
        LayoutInflater lf = getActivity().getLayoutInflater();
        View view = lf.inflate(R.layout.fragment_listing, container, false);
        background = view.findViewById(R.id.backgroundZone);
        informationCard = view.findViewById(R.id.infoCard);
        TextView foodText = view.findViewById(R.id.fragFoodTitle);
        foodText.setText(foodName);
        TextView descText = view.findViewById(R.id.fragFoodDesc);
        descText.setText(foodDescription);
        ImageView imageText = view.findViewById(R.id.fragFoodImage);
        TextView quantityText = view.findViewById(R.id.fragFoodQuantity);
        quantityText.setText("Quantity: " + foodQuantity);
        TextView locationText = view.findViewById(R.id.fragFoodLocation);
        locationText.setText(foodLocation);
        TextView dateText = view.findViewById(R.id.fragFoodDate);
        dateText.setText(foodDate);
        TextView timeText = view.findViewById(R.id.fragFoodTime);
        timeText.setText(foodTime);
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        imageText.setImageBitmap(bitmap);

        informationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Button shareButton = (Button) view.findViewById(R.id.fragShareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file_path = mContext.getExternalFilesDir(null).getAbsolutePath();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, "Demo Title");
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri path = FileProvider.getUriForFile(mContext, "com.example.task61d", new File(file_path));
                    intent.putExtra(Intent.EXTRA_STREAM, path);
                } else {
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(file_path)));
                }
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("plain/*");
                startActivity(intent);
                afdb.insertFood(new FoodItem(foodImage, username, foodName, foodDescription, foodLocation, foodQuantity, foodTime, foodDate));
                Toast.makeText(getContext(),"Successfully shared!", Toast.LENGTH_SHORT).show();
            }
        });

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container.removeView(view);
            }
        });
        return view;
    }
}