package com.example.task71p;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button createNote, showAllNote ;
    DataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNote = findViewById(R.id.createButton);
        showAllNote = findViewById(R.id.showAllButton);
        db = new DataBaseHelper(this);

        createNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createIntent = new Intent(MainActivity.this, CreateActivity.class);
                startActivity(createIntent);
            }
        });

        showAllNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (db.allNotes().size() == 0) {
                        Toast.makeText(MainActivity.this, "There are currently no Notes", Toast.LENGTH_LONG).show();
                    } else {
                        Intent showAllIntent = new Intent(MainActivity.this, ViewNotesActivity.class);
                        startActivity(showAllIntent);
                    }
            }
        });
    }
}