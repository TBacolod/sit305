package com.example.task71p;

import android.app.IntentService;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class MyIntentService extends IntentService {

    DataBaseHelper db;


    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        ArrayList<String> notesService = intent.getStringArrayListExtra("Array");
        db = new DataBaseHelper(this);

        if (db.allNotes() != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, notesService);
            ViewNotesActivity.notelist.setAdapter(adapter);

            ViewNotesActivity.notelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), ModifyActivity.class);
                    String item = ViewNotesActivity.notelist.getItemAtPosition(position).toString();
                    intent.putExtra("NoteList", item);
                    startActivity(intent);
                }
            });
        }
        else {
            Toast.makeText(this, "No notes created!", Toast.LENGTH_SHORT).show();
        }
    }

}