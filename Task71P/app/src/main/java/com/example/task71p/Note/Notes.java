package com.example.task71p.Note;

public class Notes {
    private int id;
    private String noteContent;

    public Notes(String noteContent) {
        this.noteContent = noteContent;
    }

    public Notes() {}

    public int getId() {return id;}
    public String getNote(){return noteContent;}

    public void setId(int id) {this.id = id;}
    public void setNote(String noteContent) {this.noteContent = noteContent;}
}
