package com.example.task71p;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task71p.Note.Notes;

public class CreateActivity extends AppCompatActivity {
    Button createNote;
    DataBaseHelper db;
    EditText note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        createNote = findViewById(R.id.saveButton);
        note = findViewById(R.id.noteEditText);
        db = new DataBaseHelper(this);


        createNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String noteContent = note.getText().toString();
                if (noteContent.length() > 0) {
                db.insertNote(new Notes(noteContent));
                Toast.makeText(CreateActivity.this, "Note added!", Toast.LENGTH_SHORT).show();
                finish();
                } else {
                    Toast.makeText(CreateActivity.this, "No note created!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}