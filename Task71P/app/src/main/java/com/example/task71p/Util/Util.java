package com.example.task71p.Util;

public class Util {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "notes_db";
    public static final String TABLE_NAME = "notes_table";
    public static final String NOTE_ID = "notes_id";
    public static final String NOTE_VALUES = "notes_text";
}
