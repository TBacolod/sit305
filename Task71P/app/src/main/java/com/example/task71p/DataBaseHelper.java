package com.example.task71p;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;


import androidx.annotation.Nullable;

import java.util.ArrayList;

import com.example.task71p.Note.Notes;
import com.example.task71p.Util.Util;

public class DataBaseHelper extends SQLiteOpenHelper {

    public DataBaseHelper(@Nullable Context context) {
        super(context, Util.DATABASE_NAME, null, Util.DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + Util.TABLE_NAME  + "(" + Util.NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + Util.NOTE_VALUES + " TEXT)";
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USER_TABLE = "DROP TABLE IF EXISTS";
        db.execSQL(DROP_USER_TABLE, new String[]{Util.TABLE_NAME});
        onCreate(db);
    }


    //Insert note method
    public long insertNote (Notes noteContent) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Util.NOTE_VALUES, noteContent.getNote()); //Does not need ID input as it auto increments
        long newRowID = db.insert(Util.TABLE_NAME, null, contentValues); //If null, does not insert row
        db.close();
        return newRowID;
    }

    //Delete Note Method
    public void deleteNote (String noteContent) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(" DELETE FROM " + Util.TABLE_NAME + " WHERE " + Util.NOTE_VALUES + "=\"" + noteContent.toString() + "\";" );
        db.close();
    }

    //Modify Note Method
    public int modifyNote(String noteContent, String updateText) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Util.NOTE_VALUES, updateText);
        int updates = db.update(Util.TABLE_NAME, contentValues, Util.NOTE_VALUES + "=?", new String[]{noteContent});
        db.close();
        return updates;
    }

    //Create Empty list and populate with each Database row
    public ArrayList<String> allNotes() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Util.TABLE_NAME, null, null, null,  null, null, null);
        ArrayList<String> allnotes = new ArrayList<>();
        if (cursor.getCount() > 0) {
            if (cursor != null) {
                cursor.moveToNext();

            }
            do {
                allnotes.add(cursor.getString(cursor.getColumnIndex(Util.NOTE_VALUES)));
            } while (cursor.moveToNext());
        }
        db.close();
        return allnotes;
    }
}

