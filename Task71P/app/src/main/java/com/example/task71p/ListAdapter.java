package com.example.task71p;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private List<String> stringList = new ArrayList<>();

    public ListAdapter(@NonNull Context context, ArrayList<String> list) {
        super(context, 0, list);
        mContext = context;
        stringList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);
        }

        String currentItem = stringList.get(position);
        String currentCount = String.valueOf(position + 1);
        TextView name = (TextView)listItem.findViewById(R.id.textView_name);
        name.setText(currentItem);

        TextView id = (TextView)listItem.findViewById(R.id.textView_id);
        id.setText("Note " + currentCount);

        return listItem;
    }
}
