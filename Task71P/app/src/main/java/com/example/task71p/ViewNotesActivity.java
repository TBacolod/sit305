package com.example.task71p;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ViewNotesActivity extends AppCompatActivity {

    public static ListView notelist;
    DataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notes);
        db = new DataBaseHelper(this);
        notelist = findViewById(R.id.noteList);

        ArrayList<String> notes = db.allNotes();

        ListAdapter adapter = new ListAdapter(this, notes);
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, notes);
        notelist.setAdapter(adapter);

        notelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), ModifyActivity.class);
                String item = notelist.getItemAtPosition(position).toString();
                intent.putExtra("NoteList", item);
                startActivityForResult(intent,1);
            }
        });
    }

    //On return from Modify Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (db.allNotes().size() != 0) {
            if (requestCode == 1) {
                setContentView(R.layout.activity_view_notes);
                db = new DataBaseHelper(this);
                notelist = findViewById(R.id.noteList);
                ArrayList<String> notes = db.allNotes();
                ListAdapter adapter = new ListAdapter(this, notes);
                //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, notes);
                notelist.setAdapter(adapter);

                notelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getApplicationContext(), ModifyActivity.class);
                        String item = notelist.getItemAtPosition(position).toString();
                        intent.putExtra("NoteList", item);
                        startActivityForResult(intent, 1);
                    }
                });
            }
        }
        else {
            finish();
        }
    }
}