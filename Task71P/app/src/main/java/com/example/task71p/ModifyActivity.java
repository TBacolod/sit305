package com.example.task71p;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task71p.Util.Util;

public class ModifyActivity extends AppCompatActivity {
    Button updateNote, deleteNote;
    EditText noteContent;
    DataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);
        updateNote = findViewById(R.id.updateButton);
        deleteNote = findViewById(R.id.deleteButton);
        noteContent = findViewById(R.id.noteEditText);

        Bundle bundle = getIntent().getExtras();
        noteContent.setText(bundle.getString("NoteList"));
        db = new DataBaseHelper(this);

        updateNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (noteContent.getText().length() != 0) {
                    int changes = db.modifyNote(bundle.getString("NoteList"), noteContent.getText().toString());
                    if (changes > 0) {
                        Toast.makeText(ModifyActivity.this, "Note Updated", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    db.deleteNote(bundle.getString("NoteList"));
                    Toast.makeText(ModifyActivity.this, "Note Deleted", Toast.LENGTH_SHORT).show();
                }
                setResult(1);
                finish();
            }
        });

        deleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteNote(bundle.getString("NoteList"));
                Toast.makeText(ModifyActivity.this, "Note Deleted", Toast.LENGTH_SHORT).show();
                setResult(1);
                finish();
            }
        });
    }
}