package com.example.task31c;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class QuestionsActivity extends AppCompatActivity {
    TextView textProgress, textName, optionOne, optionTwo, optionThree, textInfo, textResult;
    ProgressBar progressBar;
    boolean scoreScreen = false;
    Integer score = 0, answer = -1;
    Button nextPageBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        //Setters
        optionOne = findViewById(R.id.optionOne);
        optionTwo = findViewById(R.id.optionTwo);
        optionThree = findViewById(R.id.optionThree);
        textProgress = findViewById(R.id.textProgress);
        textName = findViewById(R.id.textName);
        textResult = findViewById(R.id.textResult);
        textInfo = findViewById(R.id.textInfo);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(1);
        textProgress.setText("1/5");
        textInfo.setText(answer.toString());

        nextPageBtn = findViewById(R.id.nextPageBtn);

        Bundle bundle = getIntent().getExtras();
        String item = bundle.getString("PlayerName");
        String name = item;
        textName.setText("Hello " + name + "!");
        textResult.setText("Android Versions");
        textInfo.setText("What are all the android version code names named after?");
        optionOne.setText("Car Brands");
        optionTwo.setText("Desserts");
        optionThree.setText("Sporting Teams");

        optionOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                optionOne.setBackgroundResource(R.drawable.selected);
                optionTwo.setBackgroundResource(R.drawable.selection);
                optionThree.setBackgroundResource(R.drawable.selection);
                answer = 1;
            }
        });
        optionTwo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                optionOne.setBackgroundResource(R.drawable.selection);
                optionTwo.setBackgroundResource(R.drawable.selected);
                optionThree.setBackgroundResource(R.drawable.selection);
                answer = 2;
            }
        });
        optionThree.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                optionOne.setBackgroundResource(R.drawable.selection);
                optionTwo.setBackgroundResource(R.drawable.selection);
                optionThree.setBackgroundResource(R.drawable.selected);
                answer = 3;
            }
        });
        nextPageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                resultScreen(view);
            }
        });
    }



    public void resultScreen(View view) {
        Intent intent = new Intent(QuestionsActivity.this, ResultActivity.class);
        if (answer > 0) { //If selected an answer
            nextQuestion(view);
        }
        else {
            Toast.makeText(this, "Please choose an answer", Toast.LENGTH_LONG).show();
        }
        if (scoreScreen == true) {
            Bundle bundle = getIntent().getExtras();
            String item = bundle.getString("PlayerName");
            intent.putExtra("PlayerName", item);
            intent.putExtra("PlayerScore", score);
            startActivityForResult(intent, 1);
        }
    }
    public void nextQuestion(View view) {
        String text = nextPageBtn.getText().toString();
        int pageNumber = progressBar.getProgress();
        if (text != "Next Question" && text != "Finish") {
            nextPageBtn.setText("Next Question");
            switch (pageNumber) {
                case 1:
                    optionTwo.setBackgroundResource(R.drawable.right);
                    if (answer == 1) {
                        optionOne.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("WRONG");
                        optionThree.setText("");
                    }
                    if (answer == 2) {
                        score++;
                        optionOne.setText("");
                        optionTwo.setText("CORRECT");
                        optionThree.setText("");
                    }
                    if (answer == 3) {
                        optionThree.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("");
                        optionThree.setText("WRONG");
                    }
                    break;
                case 2:
                    optionThree.setBackgroundResource(R.drawable.right);
                    if (answer == 1) {
                        optionOne.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("WRONG");
                        optionTwo.setText("");
                    }
                    if (answer == 2) {
                        optionTwo.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("");
                        optionTwo.setText("WRONG");
                    }
                    if (answer == 3) {
                        score++;
                        optionOne.setText("");
                        optionTwo.setText("");
                        optionThree.setText("CORRECT");
                    }
                    break;
                case 3:
                case 4:
                    optionOne.setBackgroundResource(R.drawable.right);
                    if (answer == 1) {
                        score++;
                        optionOne.setText("CORRECT");
                        optionTwo.setText("");
                        optionThree.setText("");
                    }
                    if (answer == 2) {
                        optionTwo.setBackgroundResource(R.drawable.wrong);
                        optionTwo.setText("WRONG");
                        optionThree.setText("");
                    }
                    if (answer == 3) {
                        optionThree.setBackgroundResource(R.drawable.wrong);
                        optionTwo.setText("");
                        optionThree.setText("WRONG");
                    }
                    break;
                case 5:
                    optionTwo.setBackgroundResource(R.drawable.right);
                    if (answer == 1) {
                        optionOne.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("WRONG");
                        optionThree.setText("");
                    }
                    if (answer == 2) {
                        score++;
                        optionOne.setText("");
                        optionTwo.setText("CORRECT");
                        optionThree.setText("");
                    }
                    if (answer == 3) {
                        optionThree.setBackgroundResource(R.drawable.wrong);
                        optionOne.setText("");
                        optionThree.setText("WRONG");
                    }
                    nextPageBtn.setText("Finish");
                    break;

            }

        }
        else if (text == "Finish")
        {
            scoreScreen = true;
        }

        else {
            progressBar.setProgress(progressBar.getProgress() + 1);
            textProgress.setText(progressBar.getProgress() + "/" + progressBar.getMax());
            optionOne.setBackgroundResource(R.drawable.selection);
            optionTwo.setBackgroundResource(R.drawable.selection);
            optionThree.setBackgroundResource(R.drawable.selection);
            nextPageBtn.setText("Submit");
            answer = -1;
            switch (pageNumber) {
                case 1:
                    textResult.setText("IDE");
                    textInfo.setText("What is an Integrated Development Environment");
                    optionOne.setText("A type of ecosystem");
                    optionTwo.setText("A collaboration of workload");
                    optionThree.setText("A software app that helps programmers");
                    break;
                case 2:
                    textResult.setText("Layout Styles");
                    textInfo.setText("What does a Linear Layout do?");
                    optionOne.setText("Aligns all children items in a single direction horizontally or vertically");
                    optionTwo.setText("Changes colour of text");
                    optionThree.setText("Changes size of text");
                    break;
                case 3:
                    textResult.setText("Activity LifeCycle");
                    textInfo.setText("Which one of these is not an activity state?");
                    optionOne.setText("onRemake()");
                    optionTwo.setText("onStart()");
                    optionThree.setText("onDestroy()");
                    break;
                case 4:
                    textResult.setText("Pausing Activity");
                    textInfo.setText("What would the onPause() activity be good for?");
                    optionOne.setText("When exiting application");
                    optionTwo.setText("When popup occurs");
                    optionThree.setText("When user starts application");
                    break;
            }
        }
    }
}