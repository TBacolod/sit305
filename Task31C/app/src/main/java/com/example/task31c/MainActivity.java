package com.example.task31c;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
EditText playerName;
Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerName = findViewById(R.id.playerName);
        startButton = findViewById(R.id.startButton);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            String item = bundle.getString("PlayerName");
            playerName.setText(item);
        }

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playerName.length() != 0) {
                    Intent intent = new Intent(MainActivity.this, QuestionsActivity.class);
                    String item = playerName.getText().toString();
                    intent.putExtra("PlayerName", item);
                    startActivityForResult(intent,1);
                } else {
                    Toast.makeText(MainActivity.this, "Please enter a name", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}