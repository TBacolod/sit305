package com.example.task31c;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView textName2, textTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        textName2 = findViewById(R.id.textName2);
        textTotal = findViewById(R.id.textTotal);

        Bundle bundle = getIntent().getExtras();
        String item = bundle.getString("PlayerName");
        Integer score = bundle.getInt("PlayerScore");
        textTotal.setText(score.toString() + "/5");
        switch (score)
        {
            case 0:
                String result0 = String.format("You got the worst score %s!", item);
                textName2.setText(result0);
                break;
            case 1:
                String result1 = String.format("You need to study more %s!", item);
                textName2.setText(result1);
                break;
            case 2:
                String result2 = String.format("You need to study more %s!", item);
                textName2.setText(result2);
                break;
            case 3:
                String result3 = String.format("Good Job %s! %n You got an average score!", item);
                textName2.setText(result3);
                break;
            case 4:
                String result4 = String.format("Great Job %s! %n You have a really high score.", item);
                textName2.setText(result4);
                break;
            case 5:
                String result5 = String.format("Wow %s! %n You got the highest score! Congratulations.", item);
                textName2.setText(result5);
                break;
        }
    }

    public void mainScreen(View view) {
        Intent intent = new Intent(ResultActivity.this, MainActivity.class);
        Bundle bundle = getIntent().getExtras();
        String item = bundle.getString("PlayerName");
        intent.putExtra("PlayerName", item);
        startActivityForResult(intent, 1);
    }

    public void endActivity(View view) {
        moveTaskToBack(true);
        System.exit(0);
        //this.finishAffinity();
    }
}